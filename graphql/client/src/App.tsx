import React from 'react';
import './App.css';
import TodoList from './Components/TodoList';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Graphql-react app</h1>
      </header>
      <div className="Character-wrapper">
        <TodoList />
      </div>
    </div>
  );
};

export default App;
