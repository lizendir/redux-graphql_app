import * as React from 'react';
import { Query } from 'react-apollo';
import { GET_TODOS } from '../queries/TodoQuery';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import InfiniteScroll from 'react-infinite-scroll-component';
import { LIMIT_COUNT } from './TodoList';
import { headers } from './HeaderColumns';
import { ButtonGroup, Button } from 'react-bootstrap';
import { ModalEdit } from './Modals/ModalEdit';
import { ModalCreate, TEMP_TODO } from './Modals/ModalCreate';
import { ModalDelete } from './Modals/ModalDelete';
import { Todo } from './Todo';

interface Props {
  count: number;
  hasMore: boolean;
  fetchMore: () => void;
}

interface State {
  showModalEdit: boolean;
  showModalDelete: boolean;
  showModalCreate: boolean;
  todo: Todo;
}

interface Data {
  getTodos: Todo[];
}

export interface ModalHandler {
  (isShow: boolean, type: 'EDIT' | 'DELETE' | 'CREATE', todo?: Todo): void;
}

export class MainTable extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      showModalDelete: false,
      showModalEdit: false,
      showModalCreate: false,
      todo: TEMP_TODO
    };
  }

  setNewTodo = (todo: Todo) => {
    this.setState({
      todo: todo
    });
  };

  modalHandler: ModalHandler = (
    isShow: boolean,
    type: 'EDIT' | 'DELETE' | 'CREATE',
    todo?: Todo
  ) => {
    if (todo) {
      const newTodo: Todo = {
        completed: todo.completed,
        id: todo.id,
        title: todo.title,
        userId: todo.userId
      };
      this.setState({
        todo: newTodo
      });
    }
    if (type === 'EDIT')
      this.setState({
        showModalEdit: isShow
      });
    if (type === 'DELETE')
      this.setState({
        showModalDelete: isShow
      });
    if (type === 'CREATE')
      this.setState({
        showModalCreate: isShow
      });
  };

  render() {
    const { count, fetchMore, hasMore } = this.props;
    const {
      showModalEdit,
      showModalDelete,
      showModalCreate,
      todo
    } = this.state;

    return (
      <Query<Data> query={GET_TODOS} variables={{ count: LIMIT_COUNT }}>
        {({ loading, error, data, refetch }) => {
          if (loading) return 'Loading...';
          if (error || !data) return `Error!`;

          return (
            <InfiniteScroll
              dataLength={count}
              next={fetchMore}
              hasMore={hasMore}
              loader={<h4>Loading...</h4>}
              endMessage={
                <p style={{ textAlign: 'center' }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
            >
              <ButtonGroup size="lg" style={{ padding: '10px' }}>
                <Button onClick={() => this.modalHandler(true, 'CREATE')}>
                  Create
                </Button>
              </ButtonGroup>
              <BootstrapTable
                data={data.getTodos.slice(0, count)}
                striped
                search
                bordered={false}
                keyField="id"
              >
                {headers.map(props => (
                  <TableHeaderColumn
                    {...props}
                    formatExtraData={hasDataFormat(
                      this.modalHandler,
                      props.dataFormat
                    )}
                    key={props.dataField}
                  >
                    {props.title}
                  </TableHeaderColumn>
                ))}
              </BootstrapTable>
              <ModalEdit
                isShow={showModalEdit}
                handleModalShow={this.modalHandler}
                todo={todo}
                refetch={refetch}
                setNewTodo={this.setNewTodo}
              />
              <ModalCreate
                isShow={showModalCreate}
                handleModalShow={this.modalHandler}
                refetch={refetch}
              />
              <ModalDelete
                handleModalShow={this.modalHandler}
                id={todo.id}
                isShow={showModalDelete}
                refetch={refetch}
              />
            </InfiniteScroll>
          );
        }}
      </Query>
    );
  }
}

function hasDataFormat(extraData, field?: Function) {
  if (!field) return;
  if (field.name.includes('Formatter')) return extraData;
}
