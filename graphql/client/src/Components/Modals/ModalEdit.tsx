import * as React from 'react';
import { ModalHandler } from '../MainTable';
import { Mutation } from 'react-apollo';
import { UPDATE_TODO } from '../../queries/TodoMutation';
import { Form, Button, Modal } from 'react-bootstrap';
import { Todo } from '../Todo';

interface ModalProps {
  isShow: boolean;
  handleModalShow: ModalHandler;
  todo: Todo;
  refetch: any;
  setNewTodo: (todo: Todo) => void;
}

export const ModalEdit: React.FC<ModalProps> = ({
  handleModalShow,
  isShow,
  refetch,
  setNewTodo,
  todo
}) => {
  return (
    <Mutation mutation={UPDATE_TODO}>
      {updateTodo => (
        <Modal
          show={isShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={() => handleModalShow(false, 'EDIT')}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Edit Todo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form style={{ fontSize: 20 }}>
              <Form.Group>
                <Form.Label>User Id</Form.Label>
                <Form.Control
                  placeholder="Enter user id"
                  value={todo.userId}
                  onChange={e =>
                    setNewTodo({
                      id: todo.id,
                      title: todo.title,
                      userId: e.target.value,
                      completed: todo.completed
                    })
                  }
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  value={todo.title}
                  placeholder="Enter title"
                  onChange={e =>
                    setNewTodo({
                      id: todo.id,
                      title: e.target.value,
                      userId: todo.userId,
                      completed: todo.completed
                    })
                  }
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Completed</Form.Label>
                <Form.Check
                  onChange={e =>
                    setNewTodo({
                      id: todo.id,
                      title: todo.title,
                      userId: todo.userId,
                      completed: !todo.completed
                    })
                  }
                  type="checkbox"
                  title="Check me out"
                  checked={todo.completed}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-danger"
              onClick={() => handleModalShow(false, 'EDIT')}
            >
              Close
            </Button>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-primary"
              onClick={e => {
                e.preventDefault();
                updateTodo({
                  variables: {
                    id: todo.id,
                    title: todo.title,
                    userId: todo.userId,
                    completed: todo.completed
                  }
                });
                handleModalShow(false, 'EDIT');
                refetch();
              }}
            >
              Edit
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </Mutation>
  );
};
