import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { ModalHandler } from '../MainTable';
import { Mutation } from 'react-apollo';
import { DELETE_TODO } from '../../queries/TodoMutation';

interface Props {
  isShow: boolean;
  handleModalShow: ModalHandler;
  id: string;
  refetch: any;
}

export const ModalDelete: React.FC<Props> = ({
  handleModalShow,
  id,
  isShow,
  refetch
}) => {
  return (
    <Mutation mutation={DELETE_TODO}>
      {deleteTodo => (
        <Modal
          show={isShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={() => handleModalShow(false, 'DELETE')}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Delete Todo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Do you really want to delete this Todo?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              style={{ fontSize: 20 }}
              className="btn"
              onClick={() => handleModalShow(false, 'DELETE')}
            >
              Close
            </Button>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-danger"
              onClick={e => {
                e.preventDefault();
                deleteTodo({
                  variables: {
                    id: id
                  }
                });
                handleModalShow(false, 'DELETE');
                refetch();
              }}
            >
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </Mutation>
  );
};
