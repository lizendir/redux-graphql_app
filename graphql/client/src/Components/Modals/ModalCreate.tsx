import * as React from 'react';
import { ModalHandler } from '../MainTable';
import { Mutation } from 'react-apollo';
import { ADD_TODO } from '../../queries/TodoMutation';
import { Modal, Button, Form } from 'react-bootstrap';
import { Todo } from '../Todo';

export const TEMP_TODO: Todo = {
  id: '',
  userId: '',
  title: '',
  completed: false
};

interface ModalProps {
  isShow: boolean;
  handleModalShow: ModalHandler;
  refetch: any;
}

export const ModalCreate: React.FC<ModalProps> = ({
  handleModalShow,
  isShow,
  refetch
}) => {
  const [todo, setTodo] = React.useState(TEMP_TODO);
  return (
    <Mutation mutation={ADD_TODO}>
      {addTodo => (
        <Modal
          show={isShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={() => handleModalShow(false, 'CREATE')}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Todo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form style={{ fontSize: 20 }}>
              <Form.Group>
                <Form.Label>User Id</Form.Label>
                <Form.Control
                  placeholder="Enter user id"
                  value={todo.userId}
                  onChange={e =>
                    setTodo({
                      id: todo.id,
                      title: todo.title,
                      userId: e.target.value,
                      completed: todo.completed
                    })
                  }
                ></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  value={todo.title}
                  placeholder="Enter title"
                  onChange={e =>
                    setTodo({
                      id: todo.id,
                      title: e.target.value,
                      userId: todo.userId,
                      completed: todo.completed
                    })
                  }
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Completed</Form.Label>
                <Form.Check
                  onChange={e =>
                    setTodo({
                      id: todo.id,
                      title: todo.title,
                      userId: todo.userId,
                      completed: !todo.completed
                    })
                  }
                  type="checkbox"
                  title="Check me out"
                  checked={todo.completed}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-danger"
              onClick={() => handleModalShow(false, 'CREATE')}
            >
              Close
            </Button>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-primary"
              onClick={e => {
                e.preventDefault();
                addTodo({
                  variables: {
                    id: todo.id,
                    title: todo.title,
                    userId: todo.userId,
                    completed: todo.completed
                  }
                });
                handleModalShow(false, 'CREATE');
                refetch();
              }}
            >
              Create
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </Mutation>
  );
};
