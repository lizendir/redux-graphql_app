import * as React from 'react';
import { Button } from 'react-bootstrap';
import { ModalHandler } from './MainTable';
import { Todo } from './Todo';

interface HeaderColumnProps {
  dataField: string;
  title: string;
  width?: string;
  dataFormat?: (
    cell: any,
    row: Todo,
    formatExtraData: ModalHandler,
    rowIndex: number
  ) => any;
}

export const headers: HeaderColumnProps[] = [
  {
    dataField: 'id',
    title: 'ID'
  },
  {
    dataField: 'userId',
    title: 'User ID'
  },
  {
    dataField: 'completed',
    title: 'Completed',
    width: '10%'
  },
  {
    dataField: 'title',
    title: 'Title'
  },
  {
    dataField: 'delete',
    title: '',
    dataFormat: deleteFormatter,
    width: '5%'
  },
  {
    dataField: 'edit',
    title: '',
    dataFormat: editFormatter,
    width: '5%'
  }
];

function deleteFormatter(cell, row, formatExtraData: ModalHandler, rowIndex) {
  return (
    <Button
      variant="danger"
      onClick={e => formatExtraData(true, 'DELETE', row)}
    >
      X
    </Button>
  );
}

function editFormatter(cell, row, formatExtraData: ModalHandler, rowIndex) {
  return (
    <Button variant="info" onClick={e => formatExtraData(true, 'EDIT', row)}>
      Edit
    </Button>
  );
}
