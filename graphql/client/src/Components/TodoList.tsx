import React from 'react';
import { MainTable } from './MainTable';

export const LIMIT_COUNT = 300;
const UPLOAD_COUNT = 20;
const INIT_COUNT = 20;

class TodoList extends React.Component {
  state = {
    hasMore: true,
    count: INIT_COUNT
  };

  fetchMore = () => {
    this.setState({
      hasMore: this.state.count < LIMIT_COUNT,
      count: this.state.count + UPLOAD_COUNT
    });
  };

  render() {
    const { count, hasMore } = this.state;
    return (
      <div>
        <h2>Todos</h2>
        <MainTable count={count} fetchMore={this.fetchMore} hasMore={hasMore} />
      </div>
    );
  }
}

export default TodoList;
