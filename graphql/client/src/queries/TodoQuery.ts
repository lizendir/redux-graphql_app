import gql from 'graphql-tag';

export const GET_TODOS = gql`
  query getTodos($count: Int) {
    getTodos(count: $count) {
      id
      userId
      title
      completed
    }
  }
`;
