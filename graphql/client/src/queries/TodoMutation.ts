import gql from 'graphql-tag';

export const ADD_TODO = gql`
  mutation addTodo(
    $id: String
    $userId: String
    $title: String
    $completed: Boolean
  ) {
    addTodo(id: $id, userId: $userId, title: $title, completed: $completed)
  }
`;

export const UPDATE_TODO = gql`
  mutation updateTodo(
    $id: String
    $userId: String
    $title: String
    $completed: Boolean
  ) {
    updateTodo(id: $id, userId: $userId, title: $title, completed: $completed)
  }
`;

export const DELETE_TODO = gql`
  mutation deleteTodo($id: String!) {
    deleteTodo(id: $id)
  }
`;
