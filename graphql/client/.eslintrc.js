module.exports = {
    parser: "@typescript-eslint/parser",
    extends: [
        "plugin:@typescript-eslint/recommended",
        "react-app",
        "plugin:prettier/recommended"
    ],
    plugins: [
        "@typescript-eslint",
        "react",
    ],
    rules: {
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-angle-bracket-type-assertion": "off",
        "prettier/prettier": ["error", {"singleQuote": true}], 
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/no-explicit-any": "off"
    },
    
};