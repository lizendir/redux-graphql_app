import { Context } from './models/context';
import { IResolvers } from 'apollo-server';
import { Todo } from './models/todo'

export const resolvers: IResolvers<{}, Context> = {
  Query: { 
    getTodos: (parent: any, args: {count: number}, ctx: Context) => {
      return ctx.models.todo.getTodo(args.count);
    },
    getById: (parent: any, args: {id: string}, ctx: Context) => {
      return ctx.models.todo.getById(args.id);
    },
  },
  Mutation: {
    addTodo: (parent: any, args: Todo, ctx: Context) => {
      return ctx.models.todo.addTodo(args);
    },
    updateTodo: (parent: any, args: Todo, ctx: Context) => {
      return ctx.models.todo.updateTodo(args);
    },
    deleteTodo: (parent: any, args: {id: string}, ctx: Context) => {
      return ctx.models.todo.deleteTodo(args.id);
    },
  }
};
