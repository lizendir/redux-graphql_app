import { ApolloServer } from 'apollo-server';
import { resolvers } from './resolvers';
import { schema } from './schema';
import { context } from './models/context';

import mongoose from 'mongoose';

require('dotenv').config();

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  context
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

const uri = process.env.ATLAS_URI || '';
mongoose.connect(uri, 
  {
    useNewUrlParser: true, 
    useCreateIndex: true, 
    useUnifiedTopology: true,
    useFindAndModify: false,
  }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log(`Mongodb database connection established successfully`);
})

