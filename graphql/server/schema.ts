import gql from 'graphql-tag';

export const schema = gql`
  type Todo {
    id: String
    userId: String
    title: String
    completed: Boolean
  }

  type Query {
    getTodos(count: Int): [Todo!],
    getById(id: String!): Todo
  }

  type Mutation {
    addTodo(
      id: String
      userId: String
      title: String
      completed: Boolean): String,
    updateTodo(
      id: String
      userId: String
      title: String
      completed: Boolean): String,
    deleteTodo(id: String!): String,
  }
`;
