import * as todoModel from './todo';

export interface Context {
  models: {
    todo: typeof todoModel;
  };
}
  
export const context: Context = {
  models: {
    todo: todoModel
  }
};