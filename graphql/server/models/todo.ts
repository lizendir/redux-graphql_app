import mongoose, { Schema } from 'mongoose';

export interface Todo {
  id?: string;
  userId: string;
  title: string;
  completed: boolean;
} 

export async function getTodo(count: number) {
  return await TodoModel.find()
    .then(todos => { 
      todos.forEach((value, index, array) => {
        value.id = value._id
      });
      return todos.slice(0, count)
    })
    .catch(err => 'error' + err);
};

export const addTodo = async (todo: Todo) =>  {
  const newTodo = new TodoModel(todo);

  return await newTodo.save()
    .then(() => 'Todo added successfully')
    .catch(err => 'error' + err);
}

export const deleteTodo = async (id: string) => {
  return await TodoModel.findByIdAndDelete(id)
    .catch(err => 'Error: ' + err)
    .then((todo) => {
      if(!todo) return 'Todo is not exist' 
      return 'Todo deleted';
    }
  );
}

export const updateTodo = async (updateTodo: Todo) => {
  return await TodoModel.findByIdAndUpdate(updateTodo.id, updateTodo)
    .then(todo => {
      if(!todo) return 'Todo is not exist' 
      return 'Todo updated';
    })
    .catch(err => 'Error: ' + err);
}

export const getById = async (id: string) => {
  return await TodoModel.findById(id)
    .then(todo => todo)
    .catch(err => 'Error: ' + err);
}

const todoSchema = new Schema<Todo>({
  userId: {type: String, required: true},
  title: {type: String, required: true},
  completed: {type: Boolean, required: true}
}, {
  timestamps: true,
});

export const TodoModel = mongoose.model('Todo', todoSchema, 'Todos');