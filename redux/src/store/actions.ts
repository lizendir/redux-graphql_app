import { action } from 'typesafe-actions';
import { Todo } from '../components/TodoItem';

export enum actionTypes {
  ADD = 'ADD',
  DELETE = 'DELETE',
  EDIT = 'EDIT'
}

export const todoActions = {
  add: (item: Todo) => action(actionTypes.ADD, item),
  delete: (idx: number) => action(actionTypes.DELETE, idx),
  edit: (item: Todo) => action(actionTypes.EDIT, item)
};
