import * as MyTypes from 'MyTypes';
import { actionTypes } from './actions';
import { Todo } from '../components/TodoItem';
import { combineReducers } from 'redux';

interface ITodoModel {
  list: Todo[];
  idCount: number;
}

export const initialState: ITodoModel = {
  list: [],
  idCount: 0
};

const todoReducer = (
  state: ITodoModel = initialState,
  action: MyTypes.RootAction
) => {
  switch (action.type) {
    case actionTypes.ADD: {
      const addItem: Todo = action.payload;
      addItem.id = state.idCount;
      return {
        ...state,
        list: [...state.list, addItem],
        idCount: state.idCount + 1
      };
    }
    case actionTypes.DELETE: {
      const oldList = [...state.list];
      const newList = oldList.filter(item => item.id !== action.payload);
      return {
        ...state,
        list: newList
      };
    }
    case actionTypes.EDIT: {
      const oldList = [...state.list];
      const newList = oldList.filter(item => {
        if (
          item.id === action.payload.id &&
          item.userId === action.payload.userId
        ) {
          item.completed = action.payload.completed;
          item.title = action.payload.title;
        }
        return item;
      });
      return {
        ...state,
        list: newList
      };
    }
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  todo: todoReducer
});

export default rootReducer;
