import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import * as MyTypes from 'MyTypes';
import { actionTypes } from '../store/actions';
import { Todo } from './TodoItem';
import { ModalEdit } from './ModalEdit';
import { ModalCreate } from './ModalCreate';

export const TEMP_TODO: Todo = {
  userId: 1,
  title: '',
  completed: false,
  id: 0
};

interface TodoContainerState {
  modalEditShow: boolean;
  modalCreateShow: boolean;
  editingTodo: Todo;
}

interface TodoContainerProps {
  todoList: Todo[];
  addToDo: (item: Todo) => object;
  deleteToDo: (idx: number) => object;
  editToDo: (item: Todo) => object;
}

class TodoContainer extends React.Component<
  TodoContainerProps,
  TodoContainerState
> {
  constructor(props) {
    super(props);
    this.state = {
      modalEditShow: false,
      modalCreateShow: false,
      editingTodo: TEMP_TODO
    };
  }

  componentDidMount = () => {
    fetch('https://jsonplaceholder.typicode.com/todos/')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        for (let i = 0; i < json.length; i++) {
          const todo: Todo = json[i];
          this.props.addToDo(todo);
        }
      });
  };

  handleDeleteButtonClick = (idx: number) => {
    this.props.deleteToDo(idx);
  };

  setEditingTodo = (isShow: boolean, todo: Todo = TEMP_TODO) => {
    this.setState({ editingTodo: todo });
    this.handleModalEditShow(isShow);
  };

  handleModalEditShow = (isShow: boolean) => {
    this.setState({
      modalEditShow: isShow
    });
  };

  handleModalCreateShow = (isShow: boolean) => {
    this.setState({
      modalCreateShow: isShow
    });
  };

  confirmEditing = () => {
    this.props.editToDo(this.state.editingTodo);
    this.setState({ modalEditShow: false });
  };

  confirmCreating = (todo: Todo) => {
    this.props.addToDo(todo);
    this.handleModalCreateShow(false);
  };

  handlerInputValue = (value: string) => {
    this.setState({
      editingTodo: {
        ...this.state.editingTodo,
        title: value
      }
    });
  };

  handlerCheckBox = () => {
    this.setState({
      editingTodo: {
        ...this.state.editingTodo,
        completed: !this.state.editingTodo.completed
      }
    });
  };

  render() {
    const { todoList } = this.props;
    const {
      modalEditShow: modalShow,
      modalCreateShow,
      editingTodo
    } = this.state;

    return (
      <>
        <div className="d-flex justify-content-center">
          <button
            className="btn btn-primary btn-lg"
            onClick={() => this.handleModalCreateShow(true)}
          >
            Create ToDo
          </button>
        </div>
        <BootstrapTable
          trClassName="h4"
          tableHeaderClass="h3"
          data={todoList}
          striped
          hover
          pagination
          search
        >
          <TableHeaderColumn dataSort={true} isKey dataField="id">
            ID
          </TableHeaderColumn>
          <TableHeaderColumn dataSort={true} dataField="userId">
            User ID
          </TableHeaderColumn>
          <TableHeaderColumn dataSort={true} dataField="completed">
            Completed
          </TableHeaderColumn>
          <TableHeaderColumn dataSort={true} dataField="title">
            Title
          </TableHeaderColumn>
          <TableHeaderColumn
            width="10%"
            dataFormat={deleteFormatter}
            formatExtraData={this.props.deleteToDo}
            dataField="delete"
          ></TableHeaderColumn>
          <TableHeaderColumn
            width="10%"
            dataFormat={editFormatter}
            formatExtraData={this.setEditingTodo}
            dataField="edit"
          ></TableHeaderColumn>
        </BootstrapTable>
        <ModalEdit
          isShow={modalShow}
          handleModalShow={this.handleModalEditShow}
          isComplete={editingTodo.completed}
          intupValue={editingTodo.title}
          handlerIntupValue={this.handlerInputValue}
          handlerCheckBox={this.handlerCheckBox}
          confirmEditing={this.confirmEditing}
        />
        <ModalCreate
          isShow={modalCreateShow}
          createToDo={this.confirmCreating}
          handleModalShow={this.handleModalCreateShow}
        />
      </>
    );
  }
}

function deleteFormatter(cell, row, deleteTodo) {
  return (
    <button className="wdt-100" onClick={e => deleteTodo(row.id)}>
      X
    </button>
  );
}

function editFormatter(cell, row, setEditingTodo) {
  return (
    <button className="wdt-100" onClick={e => setEditingTodo(true, row)}>
      Edit
    </button>
  );
}

const MapStateToProps = (store: MyTypes.ReducerState) => {
  return {
    todoList: store.todo.list
  };
};

const MapDispatchToProps = (dispatch: Dispatch<MyTypes.RootAction>) => ({
  addToDo: (item: Todo) => dispatch({ type: actionTypes.ADD, payload: item }),
  deleteToDo: (idx: number) =>
    dispatch({ type: actionTypes.DELETE, payload: idx }),
  editToDo: (item: Todo) => dispatch({ type: actionTypes.EDIT, payload: item })
});

export default connect(
  MapStateToProps,
  MapDispatchToProps
)(TodoContainer);
