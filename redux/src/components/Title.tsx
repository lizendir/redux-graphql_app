import * as React from 'react';

const Title: React.FC<{}> = () => {
  return (
    <div>
      <h1 className="text-center">Hello to React-Redux JSONFrontend</h1>
    </div>
  );
};

export default Title;
