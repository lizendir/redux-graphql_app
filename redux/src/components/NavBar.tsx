import * as React from 'react';
import { Navbar, Nav } from 'react-bootstrap';

const NavBar: React.FC<{}> = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand style={{ fontSize: 24 }} href="/">
          React-Redux
        </Navbar.Brand>
        <Nav style={{ fontSize: 18 }}>
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/Todo">Todo</Nav.Link>
        </Nav>
      </Navbar>
    </>
  );
};

export default NavBar;
