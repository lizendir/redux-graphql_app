import * as React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

interface ModalProps {
  isShow: boolean;
  isComplete: boolean;
  intupValue: string;
  handleModalShow: (isShow: boolean) => void;
  handlerIntupValue: (value: string) => void;
  handlerCheckBox: () => void;
  confirmEditing: () => void;
}

export class ModalEdit extends React.Component<ModalProps, {}> {
  render() {
    const {
      isShow,
      handleModalShow,
      intupValue,
      isComplete,
      handlerIntupValue,
      handlerCheckBox,
      confirmEditing
    } = this.props;

    return (
      <div>
        <Modal
          show={isShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={() => handleModalShow(false)}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Edit Todo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form style={{ fontSize: 20 }}>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  onChange={e => handlerIntupValue(e.target.value)}
                  style={{ fontSize: 20 }}
                  value={intupValue}
                  placeholder="Enter title"
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>Completed</Form.Label>
                <Form.Check
                  onChange={e => handlerCheckBox()}
                  type="checkbox"
                  title="Check me out"
                  checked={isComplete}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-danger"
              onClick={() => handleModalShow(false)}
            >
              Close
            </Button>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-primary"
              onClick={() => confirmEditing()}
            >
              Edit
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
