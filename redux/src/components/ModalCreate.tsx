import * as React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { Todo } from './TodoItem';
import { TEMP_TODO } from './TodoContainer';

interface ModalProps {
  isShow: boolean;
  handleModalShow: (isShow: boolean) => void;
  createToDo: (todo: Todo) => void;
}

interface ModalState {
  creatingToDo: Todo;
}

export class ModalCreate extends React.Component<ModalProps, ModalState> {
  state: ModalState = {
    creatingToDo: TEMP_TODO
  };

  onTitleInput = (value: string) => {
    this.setState({
      creatingToDo: {
        ...this.state.creatingToDo,
        title: value
      }
    });
  };

  onClickCheckBox = () => {
    this.setState({
      creatingToDo: {
        ...this.state.creatingToDo,
        completed: !this.state.creatingToDo.completed
      }
    });
  };

  onInputUserId = (value = '1') => {
    const userId = parseInt(value, 10);
    this.setState({
      creatingToDo: {
        ...this.state.creatingToDo,
        userId: userId
      }
    });
  };

  onCreating = () => {
    this.props.createToDo(this.state.creatingToDo);
    this.setState({ creatingToDo: TEMP_TODO });
    this.props.handleModalShow(false);
  };

  render() {
    const { isShow, handleModalShow } = this.props;
    const { creatingToDo } = this.state;
    const title = creatingToDo.title;
    const isComplete = creatingToDo.completed;

    return (
      <div>
        <Modal
          show={isShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={() => handleModalShow(false)}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Todo</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form style={{ fontSize: 20 }}>
              <Form.Group>
                <Form.Label>User Id</Form.Label>
                <Form.Control
                  as="select"
                  onChange={e => this.onInputUserId(e.currentTarget.value)}
                >
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  onChange={e => {
                    this.onTitleInput(e.target.value);
                  }}
                  style={{ fontSize: 20 }}
                  value={title}
                  placeholder="Enter title"
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Completed</Form.Label>
                <Form.Check
                  onChange={e => this.onClickCheckBox()}
                  type="checkbox"
                  title="Check me out"
                  checked={isComplete}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-danger"
              onClick={() => handleModalShow(false)}
            >
              Close
            </Button>
            <Button
              style={{ fontSize: 20 }}
              className="btn btn-primary"
              onClick={() => this.onCreating()}
            >
              Create
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
