import * as React from 'react';
import TodoContainer from './components/TodoContainer';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Title from './components/Title';
import NavBar from './components/NavBar';

export const App: React.FC<{}> = () => {
  return (
    <>
      <NavBar />
      <h1 className="text-center h1">React Redux Typescript</h1>
      <Router>
        <Route path="/" component={Title} />
        <Route path="/Todo" component={TodoContainer} />
      </Router>
    </>
  );
};
